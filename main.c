
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "bench.h"
#include "aligned_block_memcpy.h"

void dump(void *p, int64_t len)
{
	int64_t i = 0;
	/** header */
	printf("                ");
	for(i = 0; i < 16; i++) {
		printf(" %02x", (uint8_t)i);
	}

	/** dump */
	i = 0;
	while(i < len) {
		if((i % 16) == 0) {
			printf("\n%016llx", (uint64_t)p + i);
		}
		printf(" %02x", ((uint8_t *)p)[i]);
		i++;
	}
	printf("\n");
}

static inline
void demo(int block_cnt)
{
	int16_t *src = NULL, *dst = NULL;
	int64_t const len = block_cnt * sizeof(__m128i);
	int i = 0;

	posix_memalign((void **)&src, 32, 3*len);
	posix_memalign((void **)&dst, 32, 3*len);

	for(i = 0; i < 3*len/sizeof(int16_t); i++) {
		src[i] = i;
		dst[i] = -1;
	}
	dump(src, 3*len); printf("\n");
	dump(dst, 3*len); printf("\n");
	_aligned_block_memcpy((void *)dst + len, (void *)src, len);
	dump(src, 3*len); printf("\n");
	dump(dst, 3*len); printf("\n");

	free(src);
	free(dst);
}

static inline
void bench(int64_t n, int64_t cnt)
{
	int64_t size = n * sizeof(__m128i);
	int16_t *src = NULL, *dst = NULL;
	int64_t volatile i;

	bench_t b;

	posix_memalign((void **)&src, 32, size);
	posix_memalign((void **)&dst, 32, size);

	bench_init(b);
	bench_start(b);
	for(i = 0; i < cnt; i++) {
		memcpy(dst, src, size);
	}
	bench_end(b);
	printf("memcpy (%lld): %lld\n", size, bench_get(b) / 1000);

	bench_init(b);
	bench_start(b);
	for(i = 0; i < cnt; i++) {
		_aligned_block_memcpy(dst, src, size);
	}
	bench_end(b);
	printf("_aligned_block_memcpy (%lld): %lld\n", size, bench_get(b) / 1000);

	free(src);
	free(dst);
}

int main(int argc, char *argv[])
{
//	demo(3);

	bench(1, 1000000);
	bench(2, 1000000);
	bench(3, 1000000);
	bench(4, 1000000);
	bench(5, 1000000);
	bench(6, 1000000);
	bench(7, 1000000);
	bench(8, 1000000);

	return 0;
}
