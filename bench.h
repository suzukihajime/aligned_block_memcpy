
/*
 * @file bench.h
 */

#ifndef _BENCH_H
#define _BENCH_H

#include <sys/time.h>
/**
 * @struct _bench
 * @brief benchmark variable container
 */
struct _bench {
	struct timeval s;		/** start */
	int64_t a;				/** accumulator */
};
typedef struct _bench bench_t;

/**
 * @macro bench_init
 */
#define bench_init(b) { \
	memset(&(b).s, 0, sizeof(struct timeval)); \
	(b).a = 0; \
}

/**
 * @macro bench_start
 */
#define bench_start(b) { \
	gettimeofday(&(b).s, NULL); \
}

/**
 * @macro bench_end
 */
#define bench_end(b) { \
	struct timeval e; \
	gettimeofday(&e, NULL); \
	(b).a += ( (e.tv_sec  - (b).s.tv_sec ) * 1000000000 \
	         + (e.tv_usec - (b).s.tv_usec) * 1000); \
}

/**
 * @macro bench_get
 */
#define bench_get(b) ( \
	(b).a \
)

#endif /* #ifndef _BENCH_H */
/*
 * end of util.h
 */
