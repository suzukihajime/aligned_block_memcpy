
# aligned\_block\_memcpy

A memcpy implementation with Duff's device on xmm registers.


## Signature

	_aligned_block_memcpy(void *dst, void const *src, size_t size);


## Notes

* src and dst must be aligned to 16-byte boundary.
* size must be multipe of 16.


## Usage and example

Give `-msse4.1' to the compiler. See main.c for details.

	$ clang -v
	Apple LLVM version 6.1.0 (clang-602.0.53) (based on LLVM 3.6.0svn)
	Target: x86_64-apple-darwin14.4.0
	Thread model: posix
	$ clang -O3 -msse4.1 main.c
	$ ./a.out
	memcpy (16): 6114
	_aligned_block_memcpy (16): 2145
	memcpy (32): 8450
	_aligned_block_memcpy (32): 2277
	memcpy (48): 7601
	_aligned_block_memcpy (48): 3383
	memcpy (64): 10496
	_aligned_block_memcpy (64): 3197
	memcpy (80): 7586
	_aligned_block_memcpy (80): 4477
	memcpy (96): 10787
	_aligned_block_memcpy (96): 4831
	memcpy (112): 6030
	_aligned_block_memcpy (112): 4158
	memcpy (128): 7939
	_aligned_block_memcpy (128): 4719
