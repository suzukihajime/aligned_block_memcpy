
/**
 * @file aligned_block_memcpy.h
 */
#ifndef _ALIGNED_BLOCK_MEMCPY_H
#define _ALIGNED_BLOCK_MEMCPY_H

#include <smmintrin.h>
#include <stdint.h>

/**
 * @fn _aligned_block_memcpy
 *
 * @brief copy size bytes from src to dst.
 *
 * @detail
 * src and dst must be aligned to 16-byte boundary.
 * copy must be multipe of 16.
 */
#define _xmm_rd(src, n) (xmm##n) = _mm_load_si128((__m128i *)(src) + (n))
#define _xmm_wr(dst, n) _mm_store_si128((__m128i *)(dst) + (n), (xmm##n))
#define _aligned_block_memcpy(dst, src, size) { \
	/** duff's device */ \
	void *_src = (src), *_dst = (dst); \
	int64_t const _nreg = 8;		/** #xmm registers == 8 */ \
	int64_t const _tcnt = (size) / sizeof(__m128i); \
	int64_t const _offset = ((_tcnt - 1) & (_nreg - 1)) - (_nreg - 1); \
	int64_t _jmp = _tcnt & (_nreg - 1); \
	int64_t _lcnt = (_tcnt + _nreg - 1) / _nreg; \
	__m128i register xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7; \
	_src += _offset * sizeof(__m128i); \
	_dst += _offset * sizeof(__m128i); \
	switch(_jmp) { \
		case 0: do { _xmm_rd(_src, 0); \
		case 7:      _xmm_rd(_src, 1); \
		case 6:      _xmm_rd(_src, 2); \
		case 5:      _xmm_rd(_src, 3); \
		case 4:      _xmm_rd(_src, 4); \
		case 3:      _xmm_rd(_src, 5); \
		case 2:      _xmm_rd(_src, 6); \
		case 1:      _xmm_rd(_src, 7); \
		switch(_jmp) { \
			case 0:  _xmm_wr(_dst, 0); \
			case 7:  _xmm_wr(_dst, 1); \
			case 6:  _xmm_wr(_dst, 2); \
			case 5:  _xmm_wr(_dst, 3); \
			case 4:  _xmm_wr(_dst, 4); \
			case 3:  _xmm_wr(_dst, 5); \
			case 2:  _xmm_wr(_dst, 6); \
			case 1:  _xmm_wr(_dst, 7); \
		} \
				     _src += _nreg * sizeof(__m128i); \
				     _dst += _nreg * sizeof(__m128i); \
				     _jmp = 0; \
			    } while(--_lcnt > 0); \
	} \
}

#endif /* #ifndef _ALIGNED_BLOCK_MEMCPY_H */
/**
 * end of aligned_block_memcpy.h
 */
